package com.example.sabaddon.backtory;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Chronometer;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;


public class MainActivity extends BaseActivity {

    TextView accessToken ;
    Chronometer chronometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Hawk.init(mContext).build();

        //start chronometer
        chronometer.start();

        //set access token to view
        accessToken = (TextView) findViewById(R.id.accessToken);
        accessToken.setText(Hawk.get("access_token").toString());

    }

    

}
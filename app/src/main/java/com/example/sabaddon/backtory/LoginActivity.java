package com.example.sabaddon.backtory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sabaddon.backtory.Models.LoginUser.LoginUser;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends BaseActivity {

    EditText usernameLogin, passwordLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Hawk.init(mContext).build();

        bindWidget();
    }

    /**
     * bind to View
     */
    public void bindWidget() {

        usernameLogin = (EditText) findViewById(R.id.usernameLogin);
        passwordLogin = (EditText) findViewById(R.id.passwordLogin);
    }

    public void onClickLogin(View view) {
        wsLogin();
    }

    /**
     * call wbe service
     */
    public void wsLogin() {

        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("username", usernameLogin.getText().toString());
        params.put("password", passwordLogin.getText().toString());

        client.addHeader("X-Backtory-Authentication-Id", xBacktoryAuthenticationId);
        client.addHeader("X-Backtory-Authentication-Key", xBacktoryAuthenticationKeyClient);

        client.post(loginUrl, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Toast.makeText(LoginActivity.this, "Can not Set Request", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                getResponseLogin(responseString);
            }
        });

    }

    /**
     * parse the result login user
     * @param responseString
     */
    public void getResponseLogin(String responseString) {

        Gson gson = new Gson();

        LoginUser loginUser = gson.fromJson(responseString, null);

        Hawk.put("access_token", loginUser.getAccessToken());
        Hawk.put("refresh_token", loginUser.getRefreshToken());
        Hawk.put("expires_in", loginUser.getExpiresIn());
    }


}

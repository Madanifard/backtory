package com.example.sabaddon.backtory;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Hello_world on 12/16/2017.
 */

public class BaseActivity extends AppCompatActivity {

    public Context mContext = this;
    public Activity mActivity = this;

    public String baseUrl = "https://api.backtory.com";
    public String usersUrl = baseUrl + "/auth/users";
    public String loginUrl = baseUrl + "/auth/login";

    public String xBacktoryAuthenticationId = "5a34c77ce4b0303b2811a755";
    public String xBacktoryAuthenticationKeyMaster = "9d0a37adff414702beff7a28";
    public String xBacktoryAuthenticationKeyClient = "5a34c77ce4b074f61dbc47a0";


    public String debugTag = "debug";
    //https://backtory.com/documents/auth/rest/basics.html
}

package com.example.sabaddon.backtory;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sabaddon.backtory.Models.RegisterUser.RegisterUser;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orhanobut.hawk.Hawk;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class RegisterActivity extends BaseActivity {

    EditText firstName, lastName, username, password, email, phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Hawk.init(mContext).build();

        bindWidget();
    }


    /**
     * bind the Data
     */
    public void bindWidget(){

        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        email = (EditText) findViewById(R.id.email);
        phoneNumber = (EditText) findViewById(R.id.phoneNumber);

    }


    /**
     * call webService and set user in Backtory
     * @param view
     */
    public void onClickRegister(View view) {

        wsRegister();
    }

    /**
     * call webService
     */
    public void wsRegister() {


        AsyncHttpClient client = new AsyncHttpClient();

        RequestParams params = new RequestParams();
        params.put("firstName", firstName.getText().toString());
        params.put("lastName", lastName.getText().toString());
        params.put("username", username.getText().toString());
        params.put("password", password.getText().toString());
        params.put("email", email.getText().toString());
        params.put("phoneNumber", phoneNumber.getText().toString());

        client.addHeader("Content-Type", "application/json");
        client.addHeader("X-Backtory-Authentication-Id", xBacktoryAuthenticationId);

        client.post(usersUrl, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                Log.d(debugTag, "onFailure: throwable: " + throwable);
                Log.d(debugTag, "onFailure: responseString: " + responseString);
                Toast.makeText(mContext, "Can not send Request", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                getResponseRegister(responseString);
            }
        });
    }


    /**
     * for pars the Json from EW
     * @param responseString
     */
    public void getResponseRegister(String responseString) {

        Gson gson = new Gson();
        RegisterUser registerUser = gson.fromJson(responseString, null);

        Hawk.put("userId", registerUser.getUserId());
        Hawk.put("username", registerUser.getUsername());

        Intent intent = new Intent(mContext, LoginActivity.class);
        startActivity(intent);
    }

}
